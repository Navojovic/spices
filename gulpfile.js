var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(){
    return gulp.src('Spices/wwwroot/scss/*.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest('Spices/wwwroot/css'))
});
gulp.task('default', function(){
    gulp.watch('Spices/wwwroot/**/*.scss',gulp.series('sass'));
});