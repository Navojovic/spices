﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Spices.Models
{
    public class ShopModel
    {
        public int id;
        public string name;
        public string description;
        public string image;
        public int price;
        public ShopModel(int id, string name,string description, string image,int price) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.image = image;
            this.price = price;
        }
    }
}
