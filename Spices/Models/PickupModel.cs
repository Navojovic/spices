﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace Spices.Models
{
    public class PickupModel
    {
        public string name;
        public List<CityModel> city;

        public PickupModel(string name, XmlNodeList cities)
        {
            city = new List<CityModel>();
            this.name = name;
            for(int i = 0; i < cities.Count; i++)
            {
                XmlElement cy = (XmlElement)cities[i];
                city.Add(new CityModel(cy.GetAttribute("name"), cy.ChildNodes));
            }
        }
        public PickupModel()
        {
            System.Diagnostics.Debug.WriteLine("You have created a dummy object!");
        }
    }
}