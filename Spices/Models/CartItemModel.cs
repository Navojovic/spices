﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spices.Models
{
    public class CartItemModel
    {
        public ShopModel item;
        public int Quantity;
        public CartItemModel(ShopModel item, int Quantity)
        {
            this.item = item;
            this.Quantity = Quantity;
        }
    }
}
