﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace Spices.Models
{
    public class CityModel
    {
        public string name;
        public List<string> adress;
        public CityModel(string name, XmlNodeList adresses)
        {
            adress = new List<string>();
            this.name = name;
            for(int i = 0; i < adresses.Count; i++)
            {
                XmlElement ads = (XmlElement)adresses[i];
                adress.Add(ads.InnerText);
            }
        }
    }
}
