﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Text;

namespace Spices.Models
{
    public class DatabaseModel : IDisposable
    {
        bool disposed = false;
        public string host;
        public string user;
        public string password;
        public string database;
        MySqlConnection conn;
        

        public DatabaseModel(string host, string user, string password, string database)
        {
            this.host = host;
            this.user = user;
            this.password = password;
            this.database = database;
            connect();
        }
        
        public void connect()
        {
            string connstr = "server="+host+ ";uid=" + user + ";pwd=" + password + ";database=" + database + "";
            conn = new MySqlConnection(connstr);
            conn.Open();
        }
        public MySqlDataReader selectProductAll()
        {
            string sql = "SELECT * FROM products";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
        }
        public MySqlDataReader selectProduct(int id)
        {
            string sql = "SELECT * FROM products where id='"+id+"'";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
        }
        public MySqlDataReader selectPostsAll()
        {
            string sql = "SELECT * FROM posts";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
        }
        public MySqlDataReader selectPost(int id)
        {
            string sql = "SELECT * FROM posts where id='" + id + "'";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
        }
        public MySqlDataReader loginCheck(string name, string password)
        {
            SHA256 sha = SHA256.Create();
            string pass = BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(password))).Replace("-", "").ToLower();
            System.Diagnostics.Debug.WriteLine(name + " " + pass);
            string sql = "SELECT * FROM users where name='" + name + "' AND password='" + pass + "'";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
        }
        public bool createUser(string name, string password, int privileges)
        {
            SHA256 pass = SHA256.Create(password);
            string sql = "INSERT INTO users(name,password,privileges) VALUES ('"+name+ "','" + pass + "','" + privileges + "')";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool createProduct(string name, string description, string image_name, int price)
        {
            string sql = "INSERT INTO products (name,description,image,price) VALUES ('" + name + "','" + description + "','" + image_name + "'," + price+")";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool editProduct(int id, string name, string description, string image, int price)
        {
            string sql;
            if (name == string.Empty)
            {
                sql = "UPDATE products SET description='" + description + "',image='" + image + "',price=" + price + " WHERE id=" + id;
            }
            else if (description == string.Empty)
            {
                sql = "UPDATE products SET name='" + name + "',image='" + image + "',price=" + price + " WHERE id=" + id;
            }
            else if (image == string.Empty)
            {
                sql = "UPDATE products SET name='" + name + "',description='" + description + "',price=" + price + " WHERE id=" + id;

            }
            else if (price == 0)
            {
                sql = "UPDATE products SET name='" + name + "',description='" + description + "',image='" + image + "' WHERE id=" + id;
            }
            else
            {
                sql = "UPDATE products SET name='" + name + "',description='" + description + "',image='" + image + "',price=" + price + " WHERE id=" + id;
            }
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteProduct(int id)
        {
            string sql = "DELETE FROM products WHERE id="+id;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //post
        public bool createPost(string heading, string text, string image_name, string author)
        {
            string sql = "INSERT INTO posts (heading,text,image,author) VALUES ('" + heading + "','" + text + "','" + image_name + "','" + author + "')";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool editPost(int id, string heading, string text, string image, string author)
        {
            string sql;
            if (heading == string.Empty)
            {
                sql = "UPDATE posts SET text='" + text + "',image='" + image + "',author='" + author + "' WHERE id=" + id;
            }
            else if (text == string.Empty)
            {
                sql = "UPDATE posts SET heading='" + heading + "',image='" + image + "',author='" + author + "' WHERE id=" + id;
            }
            else if (image == string.Empty)
            {
                sql = "UPDATE posts SET heading='" + heading + "',text='" + text + "',author='" + author + "' WHERE id=" + id;

            }
            else if (author == string.Empty)
            {
                sql = "UPDATE posts SET heading='" + heading + "',text='" + text + "',image='" + image + "' WHERE id=" + id;
            }
            else
            {
                sql = "UPDATE posts SET heading='" + heading + "',text='" + text + "',image='" + image + "',author='" + author + "' WHERE id=" + id;
            }
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deletePost(int id)
        {
            string sql = "DELETE FROM posts WHERE id=" + id;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool createOrder(string name, string email, int tel, string adress, string tran, string pay, List<string> items)
        {
            string ite="";
            foreach(var item in items)
            {
                ite+=item + ",";
            }
            ite = ite.Remove(ite.Length - 1);
            string sql = "INSERT INTO orders (name,email,tel,address,tran,pay,items,status) VALUES ('"+name+ "','" + email + "','" + tel + "','" + adress + "','" + tran + "','" + pay + "','" + ite + "','1') ";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteOrder(int id)
        {
            string sql = "DELETE FROM orders WHERE id="+id;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.RecordsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            disposed = true;
        }

        ~DatabaseModel()
        {
            conn.Close();
            System.Diagnostics.Debug.WriteLine("Successfully deleted object");
        }
    }
}
