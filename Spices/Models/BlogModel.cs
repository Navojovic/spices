﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spices.Models
{
    public class BlogModel
    {
        public int id;
        public string heading;
        public string text;
        public string image;
        public string author;
        
        public BlogModel(int id, string heading, string text, string image, string author)
        {
            this.id = id;
            this.heading = heading;
            this.text = text;
            this.image = image;
            this.author = author;
        }
    }
}
