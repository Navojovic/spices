﻿function deleteProduct(id) {
    $.ajax({
        url: '/admin/deleteProduct',
        data: { id: id },
        success: function (data) {
            window.location.reload();
        }
    });
}
function createProduct(id) {
    let parent = document.getElementById(id);
    let input = parent.getElementsByTagName('input');
    let name = input[0].value;
    let desc = parent.getElementsByTagName('textarea')[0].value;
    let img = input[1].files[0];
    alert(img);
    let price = input[2].value;
    let fd = new FormData();
    fd.append('id', id);
    fd.append('name', name);
    fd.append('description', desc);
    fd.append('image', img);
    fd.append('price', price);
    $.ajax({
        url: '/admin/createProduct',
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        success: function (data) {
            window.location.reload();
        }
    });
}
function editProduct(id) {
    let parent = document.getElementById(id);
    console.log(parent);
    let fd = new FormData();
    let input = parent.getElementsByTagName("input");
    let name = input[1].value;
    let desc = parent.getElementsByTagName("textarea")[0].value;
    let img = input[2].files[0];
    let price = input[3].value;
    fd.append('id', id);
    fd.append('name', name);
    fd.append('description', desc);
    fd.append('image', img);
    fd.append('price', price);
    $.ajax({
        url: '/admin/editProduct',
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        success: function (data) {
            window.location.reload();
        }
    });
}
