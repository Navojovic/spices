﻿function login() {
    let form = document.getElementById("login");
    let input = form.getElementsByTagName("input");
    let username = input[0].value;
    let password = input[1].value;
    $.ajax({
        url: '/log',
        data: { username: username, password: password },
        success: function (data) {
            if (data) {
                window.location.assign("https://" + window.location.host + "/admin");
            }
        }
    });
}
function addtoCart(id) {
    $.ajax({
        url: '/addToCart',
        data: { id: id },
        success: function (data) {
            alert("Item added successfuly to cart!");
        }
    });
}
function setTranspay(id) {
    let parent = document.getElementById(id);
    let selects = parent.getElementsByTagName('select');
    let tran = selects[0].options[selects[0].selectedIndex].value;
    let pay = selects[1].options[selects[1].selectedIndex].value;
    $.ajax({
        url: '/setTranspay',
        data: {tran: tran, pay: pay},
        success: function (data) {
            window.location.assign("https://" + window.location.host + "/Home/Cart3");
        }
    });
}
function completeOrder(id) {
    let parent = document.getElementById(id);
    let inputs = parent.getElementsByTagName('input');
    let name = inputs[0].value;
    let email = inputs[1].value;
    let tel = inputs[2].value;
    let adress = inputs[3].value;
    $.ajax({
        url: '/completeOrder',
        data: { name: name, email: email, tel: tel, adress: adress },
        success: function (data) {
            alert("Order completed");
        }
    });
}