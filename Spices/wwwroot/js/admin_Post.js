﻿function createPost(id) {
    let parent = document.getElementById(id);
    let input = parent.getElementsByTagName('input');
    let heading = input[0].value;
    let text = parent.getElementsByTagName('textarea')[0].value;
    let img = input[1].files[0];
    let author = input[2].value;
    let fd = new FormData();
    fd.append('id', id);
    fd.append('heading', heading);
    fd.append('text', text);
    fd.append('image', img);
    fd.append('author', author);
    $.ajax({
        url: '/admin/createPost',
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        success: function (data) {
            window.location.reload();
        }
    });
}
function editPost(id) {
    let parent = document.getElementById(id);
    let fd = new FormData();
    let input = parent.getElementsByTagName("input");
    let heading = input[1].value;
    let text = parent.getElementsByTagName("textarea")[0].value;
    let img = input[2].files[0];
    let author = input[3].value;
    fd.append('id', id);
    fd.append('heading', heading);
    fd.append('text', text);
    fd.append('image', img);
    fd.append('author', author);
    $.ajax({
        url: '/admin/editPost',
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        success: function (data) {
            window.location.reload();
        }
    });
}
function deletePost(id) {
    $.ajax({
        url: '/admin/deletePost',
        data: { id: id },
        success: function (data) {
            window.location.reload();
        }
    });
}