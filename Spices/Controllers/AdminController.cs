﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Spices.Models;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace Spices.Controllers
{
    [Route("admin/")]
    public class AdminController : Controller
    {
        string saveDir = System.Environment.CurrentDirectory + "/wwwroot/images/";
        DatabaseModel db = new DatabaseModel("localhost", "spices", "123", "spices");
        public AdminController()
        {
        }
        public IActionResult Index()
        {

            return View();
        }
        [Route("logout")]
        public IActionResult Logout()
        {
            RedirectToAction("Index", "Home");
            return View();
        }
        [Route("products")]
        public IActionResult Products()
        {
            ViewData["Message"] = "List of all products, you can create, edit or delete them if you want.";
            List<ShopModel> items = new List<ShopModel>();
            MySqlDataReader rdr = db.selectProductAll();
            while (rdr.Read())
            {
                ShopModel item = new ShopModel(int.Parse(rdr[0].ToString()), rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), int.Parse(rdr[4].ToString()));
                items.Add(item);
            }
            return View(items);
        }
        [Route("posts")]
        public IActionResult Posts()
        {
            ViewData["Message"] = "List of all blog posts, you can create, edit or delete them if you want.";
            List<BlogModel> items = new List<BlogModel>();
            MySqlDataReader rdr = db.selectPostsAll();
            while (rdr.Read())
            {
                BlogModel item = new BlogModel(int.Parse(rdr[0].ToString()), rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), rdr[4].ToString());
                items.Add(item);
            }
            return View(items);
        }
        [Route("createProduct/")]
        [HttpPost("{name},{description},{image},{price}")]
        public void createProduct(string name, string description, IFormFile image, int price)
        {
            if (description != null && description.Length > 0)
            {
                description = description.Replace("%20", " ");
            }
            using (var stream = new FileStream(saveDir + "" + image.FileName, FileMode.Create))
            {
                image.CopyTo(stream);
            }
            db.createProduct(name, description, image.FileName, price);
        }
        [Route("editProduct/")]
        [HttpPost("{id},{name},{description},{image},{price}")]
        public void editProduct(int id, string name, string description, IFormFile image, int price)
        {
            System.Diagnostics.Debug.WriteLine(description);
            if (image != null)
            {
                using (var stream = new FileStream(saveDir + "" + image.FileName, FileMode.Create))
                {
                    image.CopyToAsync(stream);
                }
                db.editProduct(id, name, description, image.FileName, price);
            }
            else
            {
                db.editProduct(id, name, description, "", price);
            }
            
        }
        [Route("deleteProduct/")]
        [HttpPost("{id}")]
        public void deleteProduct(int id)
        {
            db.deleteProduct(id);
        }

        [Route("createPost/")]
        [HttpPost("{heading},{text},{image},{author}")]
        public void createPost(string heading, string text, IFormFile image, string author)
        {
            System.Diagnostics.Debug.WriteLine(image.FileName);
            System.Diagnostics.Debug.WriteLine(Path.GetTempFileName());
            if (text != null && text.Length > 0)
            {
                text = text.Replace("%20", " ");
            }
            using (var stream = new FileStream(saveDir + "" + image.FileName, FileMode.Create))
            {
                image.CopyToAsync(stream);
            }
            db.createPost(heading, text, image.FileName, author);
        }
        [Route("editPost/")]
        [HttpPost("{id},{heading},{text},{image},{author}")]
        public void editPost(int id, string heading, string text, IFormFile image, string author)
        {
            if (text != null && text.Length > 0)
            {
                text = text.Replace("%20", " ");
            }
            if (image != null)
            {
                using (var stream = new FileStream(saveDir + "" + image.FileName, FileMode.Create))
                {
                    image.CopyToAsync(stream);
                }
                db.editPost(id, heading, text, image.FileName, author);
            }
            else
            {
                db.editPost(id, heading, text, "", author);
            }
            

        }
        [Route("deletePost/")]
        [HttpPost("{id}")]
        public void deletePost(int id)
        {
            db.deletePost(id);
        }
        ~AdminController()
        {
            db.Dispose();
        }
    }
}