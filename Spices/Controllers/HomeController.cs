﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Spices.Models;

namespace Spices.Controllers
{
    public class HomeController : Controller
    {
        const string SessionKeyCart = "cart";
        const string SessionKeyTran = "tran";
        const string SessionKeyPay = "pay";

        DatabaseModel db = new DatabaseModel("localhost", "spices", "123", "spices");
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Information about us.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult Shop()
        {
            ViewData["Message"] = "List of all products, you can create, edit or delete them if you want.";
            List<ShopModel> items = new List<ShopModel>();
            MySqlDataReader rdr = db.selectProductAll();
            while (rdr.Read())
            {
                ShopModel item = new ShopModel(int.Parse(rdr[0].ToString()), rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), int.Parse(rdr[4].ToString()));
                items.Add(item);
            }
            return View(items);
        }
        [Route("product/{id}")]
        public IActionResult ProductDetail(int id)
        {
            MySqlDataReader rdr = db.selectProduct(id);
            ShopModel item = new ShopModel(0, null, null, null, 0);
            while (rdr.Read())
            {
                item = new ShopModel(int.Parse(rdr[0].ToString()), rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), int.Parse(rdr[4].ToString()));
            }
            return View(item);
        }
        public IActionResult Blog()
        {
            ViewData["Message"] = "Latest news in spice industry.";

            List<BlogModel> items = new List<BlogModel>();
            MySqlDataReader rdr = db.selectPostsAll();
            while (rdr.Read())
            {
                BlogModel item = new BlogModel(int.Parse(rdr[0].ToString()), rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), rdr[4].ToString());
                items.Add(item);
            }
            return View(items);
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Cart()
        {
            var cart = SessionHelper.GetObjectFromJson<List<CartItemModel>>(HttpContext.Session, "cart");
            ViewBag.cart = cart;
            return View();
        }

        public IActionResult Cart2()
        {
            ViewBag.pickup = new List<PickupModel>();
            XmlDocument doc = new XmlDocument();
            doc.Load(System.Environment.CurrentDirectory + "/wwwroot/files/pickup.xml");
            XmlNode pickup = doc.LastChild;
            PickupModel p = new PickupModel();
            for (int i = 0; i < pickup.ChildNodes.Count; i++)
            {
                XmlElement place = (XmlElement)pickup.ChildNodes[i];
                p = new PickupModel(place.GetAttribute("name"), place.ChildNodes);

                System.Diagnostics.Debug.WriteLine(p.name);
                for (int j = 0; j < p.city.Count; j++)
                {
                    System.Diagnostics.Debug.WriteLine(p.city[j].name);
                    for (int k = 0; k < p.city[j].adress.Count; k++)
                    {
                        System.Diagnostics.Debug.WriteLine(p.city[j].adress[k]);
                    }
                }
                ViewBag.pickup.Add(p);
            }

            return View();
        }
        public IActionResult Cart3()
        {
            var cart = SessionHelper.GetObjectFromJson<List<CartItemModel>>(HttpContext.Session, "cart");
            ViewBag.cart = cart;
            return View();
        }
        [Route("log")]
        [HttpPost("{username},{password}")]
        public bool log(string username, string password)
        {
            System.Diagnostics.Debug.WriteLine("I was here " + username);
            MySqlDataReader rdr = db.loginCheck(username, password);
            while (rdr.Read()) {
                System.Diagnostics.Debug.WriteLine("reader " + rdr[3].ToString());
                return true;
            }
            return false;
        }
        [Route("addToCart")]
        [HttpPost("{id}")]
        public bool addToCart(int id)
        {
            System.Diagnostics.Debug.WriteLine("adding to cart!");
            MySqlDataReader rdr = db.selectProduct(id);
            ShopModel item = new ShopModel(0, null, null, null, 0);
            while (rdr.Read())
            {
                item = new ShopModel(int.Parse(rdr[0].ToString()), rdr[1].ToString(), rdr[2].ToString(), rdr[3].ToString(), int.Parse(rdr[4].ToString()));
            }
            System.Diagnostics.Debug.WriteLine(item.name);
            if (SessionHelper.GetObjectFromJson<List<ShopModel>>(HttpContext.Session, "cart") == null)
            {
                List<CartItemModel> cart = new List<CartItemModel>();
                cart.Add(new CartItemModel(item, 1));
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            else
            {
                List<CartItemModel> cart = SessionHelper.GetObjectFromJson<List<CartItemModel>>(HttpContext.Session, "cart");
                int index = isExist(id);
                if (index != -1)
                {
                    cart[index].Quantity++;
                }
                else
                {
                    cart.Add(new CartItemModel(item, 1));
                }
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                return true;
            }
            return false;
        }
        [Route("remove")]
        [HttpPost("{id}")]
        public IActionResult Remove(int id)
        {
            List<CartItemModel> cart = SessionHelper.GetObjectFromJson<List<CartItemModel>>(HttpContext.Session, "cart");
            int index = isExist(id);
            cart.RemoveAt(index);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction("Index");
        }
        [Route("setTranspay")]
        [HttpPost("{tran},{pay}")]
        public void setTranspay(string tran, string pay)
        {
            SessionHelper.SetObjectAsJson(HttpContext.Session, "tran", tran);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "pay", pay);
        }
        [Route("completeOrder")]
        [HttpPost("{name},{email},{tel},{adress}")]
        public void completeOrder(string name, string email, int tel, string adress)
        {
            List<CartItemModel> cart = SessionHelper.GetObjectFromJson<List<CartItemModel>>(HttpContext.Session, "cart");
            List<string> items = new List<string>();
            foreach(var product in cart)
            {
                items.Add(product.item.id+"*"+product.Quantity);
            }
            db.createOrder(name, email, tel, adress, SessionHelper.GetObjectFromJson<string>(HttpContext.Session, "tran"),SessionHelper.GetObjectFromJson<string>(HttpContext.Session, "pay"), items);
        }
        private int isExist(int id)
        {
            List<CartItemModel> cart = SessionHelper.GetObjectFromJson<List<CartItemModel>>(HttpContext.Session, "cart");
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].item.id.Equals(id))
                {
                    return i;
                }
            }
            return -1;
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
